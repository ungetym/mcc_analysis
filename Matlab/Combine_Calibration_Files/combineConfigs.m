clear all;

%% Setup

% set path to the folder in which all other calibration folders are located
path_base = '../../Data/Example_Combine/';
% output file name
output_file = 'final_all.MCC_Cams';
% set single device folders
device_folders = {'device_0','device_1','device_2'};
% set extrinsics folder
extr_folder = 'extrinsics_rgb';
% set prefixes for the image file names
rgb_camera_prefix = 'kinect';
ir_camera_prefix = 'irkinect';
% set reference camera prefix - if an emtpy string is given, the first RGB
% camera will be used as reference camera
reference_prefix = 'kinect0';
% specify, whether the transformations should go INTO or FROM the reference
% camera
transform_into_reference = false;

%% Load all calibration parts

% load extrinsic RGB calibration
path_extr = append(path_base, extr_folder, '/final.MCC_Cams');
params_extr = parseMCCfile(path_extr);
extrinsics_reference_cam = params_extr{1,1};

% load intra device calibrations
params_dev = {};
reference_cam_indices = [];
for dev_idx = 1:size(device_folders,2)
    path_dev = append(path_base, device_folders{dev_idx},'/final.MCC_Cams');
    reference_cam_indices = [reference_cam_indices, size(params_dev,1)+1];
    params_dev = [params_dev; parseMCCfile(path_dev)];
end
    
% get indices for rgb and ir cameras
rgb_camera_indices = [];
ir_camera_indices = [];
for i = 1:size(params_dev,1)
    name = params_dev{i,1};
    if strcmp(name(1:size(rgb_camera_prefix,2)), rgb_camera_prefix)
        rgb_camera_indices = [rgb_camera_indices, i];
        % search for corresponding ir camera
        for j = 1:size(params_dev,1)
            name_ir = params_dev{j,1};
            if size(name_ir,2) >= size(ir_camera_prefix,2)
                if strcmp(name_ir(1:size(ir_camera_prefix,2)), ir_camera_prefix) && strcmp(name_ir(end), name(end))
                    ir_camera_indices = [ir_camera_indices, j];
                end
            end
        end
    end
end

%% Combine inter and intra device poses

for dev_idx = 1:size(device_folders,2)
    rgb_idx = rgb_camera_indices(dev_idx);
    ir_idx = ir_camera_indices(dev_idx);
    ref_idx = reference_cam_indices(dev_idx);

    % search for RGB entry in extrinsics
    rgb_extr_idx = 0;
    for i = 1:size(params_extr, 1)
        if strcmp(params_dev{rgb_idx,1}, params_extr{i,1})
            rgb_extr_idx = i;
            break;
        end
    end
    assert(rgb_extr_idx > 0, "One of the RGB cameras could not be found in the extrinsics file.");

    % calculate new poses with respect to the extrinsics reference camera
    if rgb_idx == ref_idx
        params_dev{rgb_idx,6} = params_extr{rgb_extr_idx,6};
        params_dev{ir_idx,6} = params_dev{ir_idx,6} * params_extr{rgb_extr_idx,6};
    else
        params_dev{ir_idx,6} = inv(params_dev{rgb_idx,6}) * params_extr{rgb_extr_idx,6};
        params_dev{rgb_idx,6} = params_extr{rgb_extr_idx,6};
    end
end

%% Recalculate all transformation to go from/into specified reference camera
if size(reference_prefix,2) > 0
    % get idx of desired reference camera
    ref_idx = 0;
    for i = 1:size(params_dev, 1)
        if strcmp(params_dev{i,1}, reference_prefix)
            ref_idx = i;
            break;
        end
    end
    assert(ref_idx > 0, "Unknown reference camera.");

    % get transform from desired reference to current reference
    t = inv(params_dev{ref_idx, 6});

    for i = 1:size(params_dev,1)
        params_dev{i,6} = params_dev{i,6} * t;
        if transform_into_reference
            params_dev{i,6} = inv(params_dev{i,6});
        end
    end

end

%% Save everything
writeMCCfile(append(path_base,output_file), params_dev);



function writeMCCfile(filepath, params_dev)
    file = fopen(filepath,'w');
    fprintf(file,'%s\n','$CamID $Width $Height $CamType');
    fprintf(file,'%s\n','$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 $distortion.k_3 $distortion.k_4 $distortion.k_5 $distortion.k_6');
    fprintf(file,'%s\n','$K matrix rowwise');
    fprintf(file,'%s\n\n','$Pose matrix rowwise');

    for i = 1:size(params_dev,1)
        fprintf(file,'%s %s %i %i\n','Cam', params_dev{i,1}, params_dev{i,2}, params_dev{i,3});
        fprintf(file,'%f %f %f %f %f %f %f %f\n', params_dev{i,4});
        fprintf(file,'%f %f %f %f %f %f %f %f %f\n', params_dev{i,5}');
        fprintf(file,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', params_dev{i,6}');
    end
    
end


function cam_params = parseMCCfile(filepath)
    cam_params = [];

    file = fopen(filepath);
    line = fgetl(file);

    while ischar(line)
        parts = split(line);
        if strcmp(parts{1},'Cam')
            % get name and resolution
            cam_name = parts{2};
            resolution_x = str2num(parts{3});
            resolution_y = str2num(parts{4});
            
            % read distortion parameters
            line = fgetl(file);
            parts = split(line);
            k_1 = str2num(parts{1});
            k_2 = str2num(parts{2});
            p_1 = str2num(parts{3});
            p_2 = str2num(parts{4});
            k_3 = str2num(parts{5});
            k_4 = str2num(parts{6});
            k_5 = str2num(parts{7});
            k_6 = str2num(parts{8});
            
            % read K matrix
            line = fgetl(file);
            K = transpose(reshape(str2num(line),[3,3]));
            
            % read pose
            line = fgetl(file);
            P = transpose(reshape(str2num(line),[4,4]));
            
            cam_params = [cam_params; {cam_name, resolution_x, ... 
                resolution_y, [k_1, k_2, p_1, p_2, k_3, k_4, k_5, k_6], ...
                K, P}];
        end
        line = fgetl(file);
    end
    fclose(file);
end