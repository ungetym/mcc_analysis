clear all;

%% Load intra device calibrations

% set path to single device calibration folders
path_base = '../../Data/Example_Combine/';
% set prefixes for the image file names
rgb_camera_prefix = 'kinect';
ir_camera_prefix = 'irkinect';

% load all calibrations
path_dev_0 = append(path_base, 'device_0/final.MCC_Cams');
params = parseMCCfile(path_dev_0);
path_dev_1 = append(path_base, 'device_1/final.MCC_Cams');
params = [params; parseMCCfile(path_dev_1)];
path_dev_2 = append(path_base, 'device_2/final.MCC_Cams');
params = [params; parseMCCfile(path_dev_2)];


%% Combine RGB cameras into one file

% get indices for rgb cameras
rgb_camera_indices = [];
for i = 1:size(params,1)
    name = params{i,1};
    if strcmp(name(1:size(rgb_camera_prefix,2)), rgb_camera_prefix)
        rgb_camera_indices = [rgb_camera_indices, i];
        params{i,6} = eye(4);
    end
end

params = params(rgb_camera_indices, :);

%% Save everything
writeMCCfile(append(path_base,'initial.MCC_Cams'), params);

function writeMCCfile(filepath, params)
    file = fopen(filepath,'w');
    fprintf(file,'%s\n','$CamID $Width $Height $CamType');
    fprintf(file,'%s\n','$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 $distortion.k_3 $distortion.k_4 $distortion.k_5 $distortion.k_6');
    fprintf(file,'%s\n','$K matrix rowwise');
    fprintf(file,'%s\n\n','$Pose matrix rowwise');

    for i = 1:size(params,1)
        fprintf(file,'%s %s %i %i\n','Cam', params{i,1}, params{i,2}, params{i,3});
        fprintf(file,'%f %f %f %f %f %f %f %f\n', params{i,4});
        fprintf(file,'%f %f %f %f %f %f %f %f %f\n', params{i,5}');
        fprintf(file,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n', params{i,6}');
    end
    
end

function cam_params = parseMCCfile(filepath)
    cam_params = [];

    file = fopen(filepath);
    line = fgetl(file);

    while ischar(line)
        parts = split(line);
        if strcmp(parts{1},'Cam')
            % get name and resolution
            cam_name = parts{2};
            resolution_x = str2num(parts{3});
            resolution_y = str2num(parts{4});
            
            % read distortion parameters
            line = fgetl(file);
            parts = split(line);
            k_1 = str2num(parts{1});
            k_2 = str2num(parts{2});
            p_1 = str2num(parts{3});
            p_2 = str2num(parts{4});
            k_3 = str2num(parts{5});
            k_4 = str2num(parts{6});
            k_5 = str2num(parts{7});
            k_6 = str2num(parts{8});
            
            % read K matrix
            line = fgetl(file);
            K = transpose(reshape(str2num(line),[3,3]));
            
            % read pose
            line = fgetl(file);
            P = transpose(reshape(str2num(line),[4,4]));
            
            cam_params = [cam_params; {cam_name, resolution_x, ... 
                resolution_y, [k_1, k_2, p_1, p_2, k_3, k_4, k_5, k_6], ...
                K, P}];
        end
        line = fgetl(file);
    end
    fclose(file);
end