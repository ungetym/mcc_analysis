%
% \brief plotDistance connects to cameras with a line and calculates their
%                     distance
%
% \param T_1          Camera 1 Translation
% \param T_2          Camera 2 Translation
% \param color        of the plotted line
%
function plotDistance(T_1, T_2, color)
   % plot camera position
   plot3([T_1(1) T_2(1)], [T_1(3)  T_2(3)], [T_1(2) T_2(2)]);

   % show euclidian distance next to line
   label = [num2str(sqrt((T_1-T_2)'*(T_1-T_2))/1000) ' m'];
   text(0.5*(T_1(1)+T_2(1)), ...
       0.5*(T_1(3)+T_2(3)), ...
       0.5*(T_1(2)+T_2(2)) + 10, ...
       label,'HorizontalAlignment','left','FontSize',8);
end