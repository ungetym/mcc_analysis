%
% \brief plotCamera creates a plot of a camera with given extrinsics
%
% \param R          Camera Rotation
% \param T          Camera Translation
% \param arrow_size size of plotted arrow showing the camera's viewing direction
% \param color      of the plotted arrow
% \param label      text to be shown next to the camera
%
function plotCamera(R, T, arrow_size, color, label)
   % plot camera position
   scatter3(T(1), T(3), T(2), arrow_size/10, color, 'filled');
   % calculate and plot viewing direction of the camera
   z = [0; 0; arrow_size];
   z = R*z;
   quiver3(T(1), T(3), T(2), z(1), z(3), z(2), color, 'linewidth', 2);
   % show label
   text(T(1), T(3), T(2) + arrow_size/10, label,'HorizontalAlignment',...
       'left','FontSize',8);
end