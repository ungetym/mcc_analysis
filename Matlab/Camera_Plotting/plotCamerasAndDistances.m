%
% \brief plotCamerasAndDistances creates a plot of cameras based on their 
%                                extrinsics
%
% \param t            array of 4x4 transformation matrices
% \param names        camera names
% \param arrow_size   size of plotted arrow showing the camera's viewing 
%                     direction
%
function plotCamerasAndDistances(t, names, arrow_size)

    % get the number of cameras
    num_of_cams = max(size(t));

    % extract rotations and translations from t
    for i = 1:num_of_cams
        current_transform = t{i};
        rotations{i} = current_transform(1:3,1:3);
        translations{i} = current_transform(1:3,4);
    end


    % configure camera plot
    clf % clear figure
    set(axes, 'Zdir', 'reverse')
    grid on;
    title("Camera positions");
    xlabel ("x");
    ylabel ("z");
    zlabel("y");
    hold on;

    % plot cameras
    for i = 1:num_of_cams
        plotCamera(rotations{i}, translations{i}, arrow_size, 'b', ...
            names{i});
        plotDistance(translations{i}, ...
            translations{mod(i+1,num_of_cams)+1});
    end
end