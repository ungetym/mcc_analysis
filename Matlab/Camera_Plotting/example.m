clear all

poses = {};
names = {};

% Load MCC file which also includes the names
path = '../Data/Example_PCL/final_all.MCC_Cams';
file = fopen(path);
line = fgetl(file);
counter = 1;
while ischar(line)
    % get camera name
    if (counter > 5 && mod(counter, 4) == 2 )
        temp = split(line);
        names{size(names,2)+1} = temp{2};
    % get camera pose
    elseif (counter > 8 && mod(counter, 4) == 1 )
        poses{size(poses,2)+1} = transpose(reshape(str2num(line),[4,4]));
    end
    line = fgetl(file);
    counter = counter + 1;
end
fclose(file);


% invert everything to get the original values
for i = 1:max(size(poses))
    poses{i} = poses{i}^-1;
end

% start plotting
plotCamerasAndDistances(poses, names, 500);