function pcl = colorizePCL(pcl, img, K, P)
    % transform 3D points into RGB camera coordinates
    pcl = pctransform(pcl, rigid3d(single(P')));
    
    % transform into pixel coordiantes and project onto image plane
    points = pcl.Location;
    points(:,:,1) = points(:,:,1)./points(:,:,3) * K(1,1) + K(1,3);
    points(:,:,2) = points(:,:,2)./points(:,:,3) * K(2,2) + K(2,3);
    
    % clamping
    points(:,:,1) = max(1, min(size(img,2),round(points(:,:,1))));
    points(:,:,2) = max(1, min(size(img,1),round(points(:,:,2))));
    index = sub2ind(size(img), points(:,:,2), points(:,:,1));
    
    % get color values
    offset = size(img,1) * size(img,2);
    color(:,:,1) = img(index(:,:));
    color(:,:,2) = img(offset + index(:,:));
    color(:,:,3) = img(2*offset + index(:,:));
    pcl.Color = color;

end