function cam_params = parseMCCfile(filepath)
    cam_params = [];

    file = fopen(filepath);
    line = fgetl(file);

    while ischar(line)
        parts = split(line);
        if strcmp(parts{1},'Cam')
            % get name and resolution
            cam_name = parts{2};
            resolution_x = str2num(parts{3});
            resolution_y = str2num(parts{4});
            
            % read distortion parameters
            line = fgetl(file);
            parts = split(line);
            k_1 = str2num(parts{1});
            k_2 = str2num(parts{2});
            p_1 = str2num(parts{3});
            p_2 = str2num(parts{4});
            k_3 = str2num(parts{5});
            k_4 = str2num(parts{6});
            k_5 = str2num(parts{7});
            k_6 = str2num(parts{8});
            
            % read K matrix
            line = fgetl(file);
            K = transpose(reshape(str2num(line),[3,3]));
            
            % read pose
            line = fgetl(file);
            P = transpose(reshape(str2num(line),[4,4]));
            
            cam_params = [cam_params; {cam_name, resolution_x, ... 
                resolution_y, [k_1, k_2, p_1, p_2, k_3, k_4, k_5, k_6], ...
                K, P}];
        end
        line = fgetl(file);
    end
    fclose(file);
end