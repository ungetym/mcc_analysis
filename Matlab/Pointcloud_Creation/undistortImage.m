function rectified_img = undistortImage(img, map_x, map_y, interpolation)
    % calculate displacement field
    [X,Y] = meshgrid(1:size(img,2), 1:size(img,1));
    displacement(:,:,1) = map_x - X;%max(1,min(size(img,2),map_x)) - X;
    displacement(:,:,2) = map_y - Y;%max(1,min(size(img,1),map_y)) - Y;

    if nargin == 4
        rectified_img = imwarp(img, displacement, interpolation);
    else
        rectified_img = imwarp(img, displacement);
    end
end