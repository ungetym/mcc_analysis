function pcl = depthToPCL(depth_img, map_x, map_y)
    pcl_data = zeros(size(depth_img,1), size(depth_img,2), 3);
    pcl_data(:,:,1) = map_x .* depth_img;
    pcl_data(:,:,2) = map_y .* depth_img;
    pcl_data(:,:,3) = depth_img;
    pcl = pointCloud(pcl_data);
end