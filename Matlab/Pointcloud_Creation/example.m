clear;

data_path='../../Data/Example_PCL/';
rgb_camera_prefix = 'kinect';
ir_camera_prefix = 'irkinect';
image_name='4592'; 

% colors for the pointclouds
colors = {'red', 'green', 'blue'};

% depth offsets for the images - TODO: analyze why this is necessary for
% the exemplary data!
depth_offsets = [-10,5,35];

% tracking area [x_min, x_max, y_min, y_max, z_,im, z_max]
tracking_area = [-1600, 1600, 1000, 3500, -500, 1600];

% median filtering for rectified depth image - disabled if set to 0
median_filter_size = 7;

% the depth measurements are usually faulty for black surfaces - set this
% variable to true in order to discard black pointcloud points
discard_dark_points = true;

% parse parameters
dev_params_file = append(data_path,'final_all.MCC_Cams');
dev_params = parseMCCfile(dev_params_file);

% get indices for rgb and ir cameras
rgb_camera_indices = [];
ir_camera_indices = [];
for i = 1:size(dev_params,1)
    name = dev_params{i,1};
    if strcmp(name(1:size(rgb_camera_prefix,2)), rgb_camera_prefix)
        rgb_camera_indices = [rgb_camera_indices, i];
        % search for corresponding ir camera
        for j = 1:size(dev_params,1)
            name_ir = dev_params{j,1};
            if size(name_ir,2) >= size(ir_camera_prefix,2)
                if strcmp(name_ir(1:size(ir_camera_prefix,2)), ir_camera_prefix) && strcmp(name_ir(end), name(end))
                    ir_camera_indices = [ir_camera_indices, j];
                end
            end
        end
    end
end

% select camera (pose) for pointcloud view
pose_0 = dev_params{4,6};

% select reference rgb image for color correction
color_reference = imread(append(data_path,dev_params{rgb_camera_indices(2),1},'_',image_name,'.png'));

for dev_idx = 1:1:3
    
    rgb_cam_idx = rgb_camera_indices(dev_idx);
    depth_cam_idx = ir_camera_indices(dev_idx);

    % calculate undistortion map - currently buggy due to numerical issues
    % in the iterative inverted distortion
    %iters = 5;
    %[map_x, map_y] = calcForwardUndistortMaps(dev_params{depth_cam_idx,2}, ...
    %    dev_params{depth_cam_idx,3}, dev_params{depth_cam_idx,5}, ...
    %    dev_params{depth_cam_idx,4}, iters);
    
    % calculate pointcloud
    %depth_img = imread('..Data/Example_PCL/depthkinect1_4663.png');
    %depth_img = double(depth_img);
    %depth_img = min(depth_img, 3000);
    %pcl = depthToPCL(depth_img, map_x, map_y);
    
    % workaround for problematic iterative inversion in undistort points
    depth_img = imread(append(data_path,'depth',dev_params{rgb_cam_idx,1},'_',image_name,'.png'));
    depth_img = double(depth_img);
    [map_x, map_y] = calcBackwardUndistortMap(dev_params{depth_cam_idx,2}, ...
        dev_params{depth_cam_idx,3}, dev_params{depth_cam_idx,5}, ...
        dev_params{depth_cam_idx,4}, dev_params{depth_cam_idx,5});
    depth_img_rectified = undistortImage(depth_img, map_x, map_y, "nearest");
    
    % median filter to smooth the depth noise
    if median_filter_size > 2
        depth_img_rectified = medfilt2(depth_img_rectified, [median_filter_size median_filter_size]);
    end

    pcl = rectifiedDepthToPCL(depth_img_rectified, dev_params{depth_cam_idx,5}, depth_offsets(dev_idx));
    
    % load corresponding color image and rectify it
    rgb_img = imread(append(data_path,dev_params{rgb_cam_idx,1},'_',image_name,'.png'));
    [map_x, map_y] = calcBackwardUndistortMap(dev_params{rgb_cam_idx,2}, ...
        dev_params{rgb_cam_idx,3}, dev_params{rgb_cam_idx,5}, ...
        dev_params{rgb_cam_idx,4}, dev_params{rgb_cam_idx,5});
    rgb_img_rectified = undistortImage(rgb_img, map_x, map_y);

    % TODO: color correction
    %rgb_img_rectified = colorCorrection(color_reference, rgb_img_rectified, 20, 5);
    
    %figure(1);
    %imshow(rgb_img_rectified);

    % color the pointcloud
    P_depth = dev_params{depth_cam_idx,6};
    P_rgb = dev_params{rgb_cam_idx,6};
    P = P_rgb * inv(P_depth); % transform from depth into rgb frame
    pcl = colorizePCL(pcl, rgb_img_rectified, dev_params{rgb_cam_idx,5}, P);
    
    % transform pointcloud into world coordinates (or pose_0's camera
    % coordinates) and flip some coodinates for viewing
    pcl = pctransform(pcl, rigid3d(single(pose_0 * inv(P_rgb))'));
    pcl = pctransform(pcl, rigid3d([1 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 1]));
    
    % pcl clamping for viewing purposes
    data = pcl.Location;
    keep = data(:,:,1) > tracking_area(1) & data(:,:,1) < tracking_area(2) ...
        & data(:,:,2) > tracking_area(3) & data(:,:,2) < tracking_area(4) ...
        & data(:,:,3) > tracking_area(5) & data(:,:,3) < tracking_area(6);
    
    if discard_dark_points
        keep = keep & max(pcl.Color, [], 3) > 20;
    end

    % set false values in keep to nan in order to be able to use
    % removeInvalidPoints
    keep = double(keep);
    keep(keep == 0) = nan;

    data(:,:,1) = keep .* data(:,:,1);
    data(:,:,2) = keep .* data(:,:,2);
    data(:,:,3) = keep .* data(:,:,3);

    pcl = pointCloud(data,"Color",pcl.Color);
    pcl.removeInvalidPoints();
    
    % show colored pcl
    figure(2);
    hold on;
    pcshow(pcl, "MarkerSize", 32);

    % show monocolor pcl for evaluation of the pcl poses
    figure(3);
    hold on;
    pcshow(pcl.Location, colors{dev_idx});
end