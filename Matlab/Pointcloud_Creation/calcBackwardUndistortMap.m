function [map_x, map_y] = calcBackwardUndistortMap(target_res_x, target_res_y, K_in, dist_coeffs, K_target)

    % calculate backward undistortion maps
    f_x = K_in(1,1);
    f_y = K_in(2,2);
    c_x = K_in(1,3);
    c_y = K_in(2,3);
    
    k_1 = dist_coeffs(1);
    k_2 = dist_coeffs(2);
    p_1 = dist_coeffs(3);
    p_2 = dist_coeffs(4);
    k_3 = dist_coeffs(5);
    k_4 = dist_coeffs(6);
    k_5 = dist_coeffs(7);
    k_6 = dist_coeffs(8);
    
    [X,Y] = meshgrid(1:target_res_x, 1:target_res_y);
    
    % apply inverse of input K
    X = (X - c_x) ./ f_x; %(target_res_x+1)/2) ./ f_x;
    Y = (Y - c_y) ./ f_y; %(target_res_y+1)/2) ./ f_y;
    
    % distort
    XX = X.*X;
    YY = Y.*Y;
    XY = X.*Y;
    R2 = XX + YY;
    
    DELTA_X = 2*p_1*XY + p_2*(R2 + 2*XX);
    DELTA_Y = p_1*(R2 + 2*YY) + 2*p_2*XY;
    DIST = (1 + ((k_3*R2 + k_2) .* R2 + k_1) .* R2) ./...
        (1 + ((k_6*R2 + k_5) .* R2 + k_4) .* R2);
    X_ = X.*DIST + DELTA_X;
    Y_ = Y.*DIST + DELTA_Y;
    
    
    if nargin == 5
        % apply target K matrix
        f_x = K_target(1,1);
        f_y = K_target(2,2);
        c_x = K_target(1,3);
        c_y = K_target(2,3);
        map_x = X_*f_x + c_x;
        map_y = Y_*f_y + c_y;
    else
        % do not apply any K matrix to keep data in world units
        map_x = X_;
        map_y = Y_;
    end
end