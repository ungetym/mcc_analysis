function [map_x, map_y] = calcForwardUndistortMaps(res_x, res_y, K, dist_coeffs, num_iter)

    f_x = K(1,1);
    f_y = K(2,2);
    c_x = K(1,3);
    c_y = K(2,3);
    
    k_1 = dist_coeffs(1);
    k_2 = dist_coeffs(2);
    p_1 = dist_coeffs(3);
    p_2 = dist_coeffs(4);
    k_3 = dist_coeffs(5);
    k_4 = dist_coeffs(6);
    k_5 = dist_coeffs(7);
    k_6 = dist_coeffs(8);
    
    [X,Y] = meshgrid(1:res_x, 1:res_y);
    X = (X - c_x) ./ f_x;
    Y = (Y - c_y) ./ f_y;
    
    X_0 = X;
    Y_0 = Y;
    
    for i = 1:num_iter
        XX = X.*X;
        YY = Y.*Y;
        XY = X.*Y;
        R2 = XX + YY;
        ICDIST = (1 + ((k_6*R2 + k_5) .* R2 + k_4) .* R2) ./ ...
            (1 + ((k_3*R2 + k_2) .* R2 + k_1) .* R2);
        DELTA_X = 2*p_1*XY + p_2*(R2 + 2*XX);
        DELTA_Y = p_1*(R2 + 2*YY) + 2*p_2*XY;
        TEST = (ICDIST<0);
        X = TEST .* X_0 + (1- TEST).*(X_0 - DELTA_X) .* ICDIST;
        Y = TEST .* Y_0 + (1- TEST).*(Y_0 - DELTA_Y) .* ICDIST;
    end
    
    map_x = X;
    map_y = Y;
    
%     % inefficient version: double loop similar to opencv's implementation
%     % in undistort.cpp
%     
%     % initialize maps
%     map_x = zeros(res_x, res_y);
%     map_y = zeros(res_x, res_y);
%     
%     for col = 1:res_x
%         for row = 1:res_y
%    
%             % revert multiplication with K
%             x = (col - c_x) / f_x;
%             y = (row - c_y) / f_y;
%             
%             x_0 = x;
%             y_0 = y;
% 
%             % compensate distortion iteratively - compare opencv's
%             % undistortpoints method
%             for i = 1:num_iter
%                 r2 = x*x + y*y;
%                 icdist = (1 + ((k_6*r2 + k_5)*r2 + k_4)*r2)/(1 + ((k_3*r2 + k_2)*r2 + k_1)*r2);
%                 delta_x = 2*p_1*x*y + p_2*(r2 + 2*x*x);
%                 delta_y = p_1*(r2 + 2*y*y) + 2*p_2*x*y;
%                 x = (x_0 - delta_x) * icdist;
%                 y = (y_0 - delta_y) * icdist;
%             end
%             
%             map_x(row, col) = x;
%             map_y(row, col) = y;
%          end
%     end
end