function pcl = rectifiedDepthToPCL(depth_img, K, depth_offset)

    [target_res_x, target_res_y] = size(depth_img);
    [X,Y] = meshgrid(1:target_res_x, 1:target_res_y);
    
    % apply inverse of input K
    f_x = K(1,1);
    f_y = K(2,2);
    c_x = K(1,3);
    c_y = K(2,3);
    X = (X - c_x) ./ f_x;
    Y = (Y - c_y) ./ f_y;
    
    pcl_data = zeros(size(depth_img,1), size(depth_img,2), 3);
    depth_img = depth_img - depth_offset;
    pcl_data(:,:,1) = X .* depth_img;
    pcl_data(:,:,2) = Y .* depth_img;
    pcl_data(:,:,3) = depth_img;
    pcl = pointCloud(pcl_data);
end