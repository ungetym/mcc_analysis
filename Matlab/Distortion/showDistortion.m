clear all

data_path='../../Data/Example_PCL/';
rgb_camera_prefix = 'kinect';
ir_camera_prefix = 'irkinect';

% parse parameters
dev_params_file = append(data_path,'final_all.MCC_Cams');
dev_params = parseMCCfile(dev_params_file);

% colors for the pointclouds
colors = {'red', 'green', 'blue'};

% get indices for rgb and ir cameras
rgb_camera_indices = [];
ir_camera_indices = [];
for i = 1:size(dev_params,1)
    name = dev_params{i,1};
    if strcmp(name(1:size(rgb_camera_prefix,2)), rgb_camera_prefix)
        rgb_camera_indices = [rgb_camera_indices, i];
        % search for corresponding ir camera
        for j = 1:size(dev_params,1)
            name_ir = dev_params{j,1};
            if size(name_ir,2) >= size(ir_camera_prefix,2)
                if strcmp(name_ir(1:size(ir_camera_prefix,2)), ir_camera_prefix) && strcmp(name_ir(end), name(end))
                    ir_camera_indices = [ir_camera_indices, j];
                end
            end
        end
    end
end

for i = 1:3

    rgb_intr = dev_params{rgb_camera_indices(i),4};
    depth_intr = dev_params{ir_camera_indices(i),4};

    %figure('Name',"RGB Distortion");
    figure(1);
    hold on;
    plotIntr(rgb_intr, [0,3], colors{i});
    %figure('Name',"Depth Distortion");
    %hold on;
    %plotIntr(depth_intr, [0,20], colors{i});

end

function plotIntr(params, range, color)
    k1 = params(1);
    k2 = params(2);
    p1 = params(3);
    p2 = params(4);
    k3 = params(5);
    k4 = params(6);
    k5 = params(7);
    k6 = params(8);

    x = range(1):(range(2)-range(1))/1000:range(2);
    y = (1.+k1*x.^2.+k2*x.^4.+k3*x.^6)./(1.+k4*x.^2.+k5*x.^4.+k6*x.^6);
    
    plot(x,y,"Color",color);
end

function cam_params = parseMCCfile(filepath)
    cam_params = [];

    file = fopen(filepath);
    line = fgetl(file);

    while ischar(line)
        parts = split(line);
        if strcmp(parts{1},'Cam')
            % get name and resolution
            cam_name = parts{2};
            resolution_x = str2num(parts{3});
            resolution_y = str2num(parts{4});
            
            % read distortion parameters
            line = fgetl(file);
            parts = split(line);
            k_1 = str2num(parts{1});
            k_2 = str2num(parts{2});
            p_1 = str2num(parts{3});
            p_2 = str2num(parts{4});
            k_3 = str2num(parts{5});
            k_4 = str2num(parts{6});
            k_5 = str2num(parts{7});
            k_6 = str2num(parts{8});
            
            % read K matrix
            line = fgetl(file);
            K = transpose(reshape(str2num(line),[3,3]));
            
            % read pose
            line = fgetl(file);
            P = transpose(reshape(str2num(line),[4,4]));
            
            cam_params = [cam_params; {cam_name, resolution_x, ... 
                resolution_y, [k_1, k_2, p_1, p_2, k_3, k_4, k_5, k_6], ...
                K, P}];
        end
        line = fgetl(file);
    end
    fclose(file);
end