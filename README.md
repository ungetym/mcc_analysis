# MCC_Analysis

A bunch of useful Matlab scripts (Matlab folder) and C++ pendants (Cpp folder) to combine, visualize and check the results of https://gitlab.com/ungetym/Multi_Camera_Calibration

## Contents - Matlab

**Matlab/Combine_Calibration_Files** contains scripts which can be used to combine multiple intra and/or inter device calibration files (.MCC_Cams files from the repo mentioned above).

**Matlab/Camera_Plotting** can be used to visualize the calibrated cameras' position and orientation in 3D. See example.m for usage.

**Matlab/Pointcloud_Creation** contains scripts to rectify RGB and depth images and use them to generate pointclouds. See example.m for usage.

**Matlab/Distortion** contains a script to plot the rational part of the Brown-Conrady distortion model in order to analyze whether the zero crossing happens within the sensor diameter.

## Contents - C++

All tools feature a CMakeLists.txt and don't require additional dependencies apart from the standard library.

**Cpp/Combine_Calibration_Files** contains a small C++ tool to combine multiple intra and/or inter device calibration files (.MCC_Cams files from the repo mentioned above). 
Usage: Call the executable with parameter -h in order to show an exemplary command and the command line documentation.
Dependencies: None apart from the C++ standard library.

**Cpp/Pointcloud_Creation** contains a small C++ tool which creates a colored .ply pointcloud file based on the Kinect Azure SDK with our own calibration values.
Usage: Change the paths and parameters specified in the first section of the main function in order use your own data. The .ply files are then stored in the specified output folder after running the executable.
Dependencies: OpenCV >=4, Kinect Azure SDK, PCL 1.3, (Eigen - this should be part of PCL)
Further notes:

- This tool relies on a MCC_Cams file containing the IR and RGB cameras. The transform of a camera is assumed to go from that camera into the reference camera frame (usually the first camera listed in the file). In order to create such a calibration file from the single calibration files (device intrinsics + separate extrinsics) you can use the scripts in `Matlab/Combine_Calibration_Files` or the tool in `Cpp/Combine_Calibration_Files`.
- The code will run with any number of cameras as long as it is listed in the calibration file and there exist an rgb and a depth image for it.
- The rgb image of a device kinectXY are assumed to be named `kinectXY_NUMBER.png` and the depth images `depthkinectXY_NUMBER.png`.
- The `libdepthengine.so.2.0` required by the Kinect Azure SDK is not available for some Linux distros. The folder `libdepthengine` contains this library and should be linked in the last line of the CMakeLists.txt.
