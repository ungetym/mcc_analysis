#include "helper.h"

#include <iostream>
#include <map>
#include <memory>
#include <string.h>

using namespace std;

struct Camera{
    string name;
    int resolution_x = 0;
    int resolution_y = 0;
    vector<float> intrinsics = vector<float>(8,0);
    vector<float> K = vector<float>(9,0);
    vector<float> pose = vector<float>(16,0);
};

struct Device{
    string name;
    Camera ir_cam;
    Camera rgb_cam;
};

///
/// \brief readDevices
/// \param input_file
/// \param devices
/// \return
///
bool readDevice(const string& input_file, vector<Device>& devices){

    ifstream file(input_file);
    if(!file.is_open()){
        cerr << "Input file "<< input_file <<" could not be opened!\n";
        return false;
    }

    // index of current line in parameter file - line 6 is the start of the
    // first camera's description
    int current_line = 6;

    // save index for every camera - this is helpful if the ir and rgb cameras
    // are not stored in a known order
    map<string,int> name_index;

    // start reading
    while(goToLine(file, current_line)){

        // extract camera name
        string device_name, camera_name;
        file >> camera_name;
        file >> camera_name;

        if(file.eof()){
            break;
        }

	int res_x, res_y;
	file >> res_x >> res_y;

        // check if ir or rgb camera
        string prefix = camera_name.substr(0,2);
        bool is_ir_camera = (prefix.compare("ir") == 0);
        device_name = is_ir_camera ? camera_name.substr(2) : camera_name;

        // search for camera name in existing cameras
        int idx = -1;
        if(name_index.count(device_name) > 0){
            idx = name_index[device_name];
        }
        else{ // if camera is not found, create a new one
            Device new_device;
            new_device.name = device_name;
            devices.push_back(new_device);
            idx = devices.size()-1;
            name_index[device_name] = idx;
        }

        float var;
        Camera& cam = is_ir_camera ? devices[idx].ir_cam : devices[idx].rgb_cam;
        cam.name = camera_name;
	cam.resolution_x = res_x;
	cam.resolution_y = res_y;
        // jump to next line in which the camera intrinsics should be located
        goToLine(file, current_line+1);
        // read ir intrinsics
        for(size_t i = 0; i < 8; i++){
            file >> cam.intrinsics[i];
        }
        // read K matrix
        for(size_t i = 0; i < 9; i++){
            file >> cam.K[i];
        }
        // read pose
        for(size_t i = 0; i < 16; i++){
            file >> cam.pose[i];
        }

        current_line += 4;
    }

    file.close();

    return true;
}

///
/// \brief writeDevices
/// \param devices
/// \param output_file
/// \param mode
/// \return
///
bool writeDevices(const vector<Device>& devices, const string& output_file, const int& mode){

    // open target file
    ofstream file(output_file);
    if(!file.is_open()){
        cerr << "Output file "<< output_file <<" could not be opened!\n";
        return false;
    }

    // write header
    file << "$CamID $Width $Height $CamType\n"
            "$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 $distortion.k_3 $distortion.k_4 $distortion.k_5 $distortion.k_6\n"
            "$K matrix rowwise\n"
            "$Pose matrix rowwise\n";

    // write devices
    for(const Device& device: devices){
        for(int i = 0; i < 2; i++){
            if(mode == 1 && i == 0){//write only rgb
                continue;
            }
            else if(mode == 2 && i == 1){//write only ir
                continue;
            }

            const Camera& cam = (i==0) ? device.ir_cam : device.rgb_cam;

            // write camera name and resolution
            file << "\nCam " << cam.name << " " << cam.resolution_x
                 << " " << cam.resolution_y << "\n";
            // write ir intrinsics
            for(size_t i = 0; i < 7; i++){
                file << cam.intrinsics[i] << " ";
            }
            file << cam.intrinsics[7] << "\n";
            // write K matrix
            for(size_t i = 0; i < 8; i++){
                file << cam.K[i] << " ";
            }
            file << cam.K[8] << "\n";
            // write pose
            for(size_t i = 0; i < 15; i++){
                file << cam.pose[i] << " ";
            }
            file << cam.pose[15];
        }
    }

    file.close();

    return true;
}

///
/// \brief combineIntrinsicsAndExtrinsics
/// \param devices_extrinsic
/// \param devices
/// \return
///
bool combineIntrinsicsAndExtrinsics(const vector<Device>& devices_extrinsic, vector<Device>& devices){

    if(devices_extrinsic.size() != devices.size()){
        cerr << "ERROR: Number of single devices does not equal number of devices in extrinsic calibration file!\n";
        return false;
    }

    for(const Device& device_ext : devices_extrinsic){
        string name_ext = device_ext.name;
        // find same device among the single devices
        for(Device& device : devices){
            if(name_ext.compare(device.name) == 0){
                // device found, now multiply the transforms
                bool ir_is_ref = (device.ir_cam.pose == std::vector<float>{1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1});
                // check, whether the ir or rgb camera was used for extrinsics estimation
                bool use_rgb_for_ext = (device_ext.ir_cam.pose == vector<float>(16,0));

                if(ir_is_ref){
                    if(use_rgb_for_ext){
                        std::vector<float> T_rgb_to_ir = invertTransform(device.rgb_cam.pose);
                        device.ir_cam.pose = matProduct(T_rgb_to_ir, device_ext.rgb_cam.pose);
                        device.rgb_cam.pose = device_ext.rgb_cam.pose;
                    }
                    else{
                        device.ir_cam.pose = device_ext.ir_cam.pose;
                        device.rgb_cam.pose = matProduct(device.rgb_cam.pose, device_ext.ir_cam.pose);
                    }
                }
                else{// rgb camera is reference for single devices
                    if(use_rgb_for_ext){
                        device.ir_cam.pose = matProduct(device.ir_cam.pose, device_ext.rgb_cam.pose);
                        device.rgb_cam.pose = device_ext.rgb_cam.pose;
                    }
                    else{
                        std::vector<float> T_ir_to_rgb = invertTransform(device.ir_cam.pose);
                        device.ir_cam.pose = device_ext.ir_cam.pose;
                        device.rgb_cam.pose = matProduct(T_ir_to_rgb, device_ext.ir_cam.pose);
                    }
                }
            }
        }
    }

    return true;
}

///
/// \brief main
/// \param argc
/// \param argv
/// \return
///
int main(int argc, char *argv[]){

    string help_text = "Usage: \n"
                       "CombineMCCFiles [MODE] [INPUTFILE1] [INPUTFILE2].. [OUTPUTFILE]\n"
                       "Example: \n"
                       "CombineMCCFiles -i ../Data/device0.MCC_Cams ../Data/device1.MCC_Cams ../Data/output.MCC_Cams\n"
                       "Description:\n"
                       "[Mode]         -i for combination of multiple intrinsic calibrations\n"
                       "               -e for combination of multiple inter and intra device calibrations\n"
                       "[INPUTFILE..]  Path to an input file. At least 2 input files need to be specified "
                       "in order to perform a combination.\n"
                       "[OUTPUTFILE..] Path to the output file. Will be created if not existent.\n";

    // Check if the help text was requested or the general number of arguments
    // is incorrect
    if(argc == 2 && strcmp(argv[1],"-h") == 0 ){
        cout << help_text;
        return 0;
    }
    else if(argc < 5){
        cerr << "Error: Insufficient number of arguments!\n";
        cout << help_text;
        return 0;
    }

    // Parse the requested combination mode: -i for intrinsic combination and -e
    // for combined intrinsic and extrinsic combination
    int mode = -1;
    if(strcmp(argv[1],"-i") == 0){
        mode = 0;
        cout << "Selected combination of single devices.\n";
    }
    else if(strcmp(argv[1],"-e") == 0){
        mode = 1;
        cout << "Selected combination of intrinsics and extrinsics.\n";
    }
    if(mode == -1){
        cerr << "ERROR: First argument should be -i or -e.\n\n";
        cout << help_text;
        return 0;
    }

    // Check whether the input files exist and can be opened
    vector<string> input_files;
    for(int arg_idx = 2; arg_idx < argc-1; arg_idx++){
        string input_file_name = argv[arg_idx];
        ifstream input_file(input_file_name);
        if(!input_file.is_open()){
            cerr << "ERROR: Input file "<<input_file_name<<" could not be opened.";
            return 0;
        }
        input_file.close();
        input_files.push_back(input_file_name);
    }
    cout << "All input files OK.\n";

    // Check whether the output file exists and create a new one, if not
    string output_file_name = argv[argc-1];
    ofstream output_file(output_file_name);
    if(!output_file.is_open()){
        cerr << "ERROR: Output file "<<output_file_name<<" could not be opened or created.";
        return 0;
    }
    output_file.close();
    cout << "Output file OK.\n";

    // Start parsing the input files
    cout << "Parsing the single device files...\n";
    vector<Device> devices;
    size_t num_input_files = (mode == 0) ? input_files.size() : input_files.size()-1;
    for(size_t i = 0; i < num_input_files; i++){
        vector<Device> new_devices;
        if(!readDevice(input_files[i], new_devices)){
            cerr << "ERROR: Input file could not be parsed - check the correct file formatting!\n";
            return 0;
        }
        devices.insert(devices.end(), new_devices.begin(), new_devices.end());
    }
    cout << "...done.\n";

    // Combine parameters if necessary
    if(mode == 1){
        cout << "Parsing the extrinsics files...\n";
        vector<Device> devices_extrinsics;
        if(!readDevice(input_files.back(), devices_extrinsics)){
            cerr << "ERROR: Extrinsics input file could not be parsed - check the correct file formatting!\n";
            return 0;
        }
        cout << "...done.\n";

        cout << "Combining parameters...\n";
        if(!combineIntrinsicsAndExtrinsics(devices_extrinsics, devices)){
            cerr << "ERROR: Combination of extrinsics and intrinsics failed!";
            return 0;
        }
        cout << "...done.\n";
    }

    // Writing to output file
    cout << "Writing to output file...\n";
    int write_mode = 0;
    if(mode == 0){
        if(devices[0].ir_cam.pose == std::vector<float>{1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1}){
            write_mode = 1; //write only rgb
        }
        else{
            write_mode = 2; //write only ir
        }
    }
    if(!writeDevices(devices, output_file_name, write_mode)){
        cerr << "ERROR: Output file could not be written!";
        return 0;
    }
    cout << "...done.\n";
    cout << "The combination was successfull!\n";

    return 0;
}
