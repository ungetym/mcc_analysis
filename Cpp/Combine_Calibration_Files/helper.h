#pragma once

#include <fstream>
#include <vector>

//////////////////////// matrix/vector calculations ////////////////////////////

///
/// \brief matProduct   calculates the product of two 3x3 or 4x4 matrices
/// \param A            3x3 or 4x4 matrix, saved row-wise
/// \param B            3x3 or 4x4 matrix, saved row-wise
/// \return             A*B, row-wise
///
std::vector<float> matProduct(const std::vector<float>& A, const std::vector<float>& B){
    std::vector<float> result;
    if(A.size() == 9 && B.size()==9){
        result.push_back(A[0]*B[0]+A[1]*B[3]+A[2]*B[6]);
        result.push_back(A[0]*B[1]+A[1]*B[4]+A[2]*B[7]);
        result.push_back(A[0]*B[2]+A[1]*B[5]+A[2]*B[8]);
        result.push_back(A[3]*B[0]+A[4]*B[3]+A[5]*B[6]);
        result.push_back(A[3]*B[1]+A[4]*B[4]+A[5]*B[7]);
        result.push_back(A[3]*B[2]+A[4]*B[5]+A[5]*B[8]);
        result.push_back(A[6]*B[0]+A[7]*B[3]+A[8]*B[6]);
        result.push_back(A[6]*B[1]+A[7]*B[4]+A[8]*B[7]);
        result.push_back(A[6]*B[2]+A[7]*B[5]+A[8]*B[8]);
    }
    else if(A.size() == 16 && B.size() == 16){
        std::vector<float> AB = {A[0]*B[0]+A[1]*B[4]+A[2]*B[8]+A[3]*B[12],
                                 A[0]*B[1]+A[1]*B[5]+A[2]*B[9]+A[3]*B[13],
                                 A[0]*B[2]+A[1]*B[6]+A[2]*B[10]+A[3]*B[14],
                                 A[0]*B[3]+A[1]*B[7]+A[2]*B[11]+A[3]*B[15],

                                 A[4]*B[0]+A[5]*B[4]+A[6]*B[8]+A[7]*B[12],
                                 A[4]*B[1]+A[5]*B[5]+A[6]*B[9]+A[7]*B[13],
                                 A[4]*B[2]+A[5]*B[6]+A[6]*B[10]+A[7]*B[14],
                                 A[4]*B[3]+A[5]*B[7]+A[6]*B[11]+A[7]*B[15],

                                 A[8]*B[0]+A[9]*B[4]+A[10]*B[8]+A[11]*B[12],
                                 A[8]*B[1]+A[9]*B[5]+A[10]*B[9]+A[11]*B[13],
                                 A[8]*B[2]+A[9]*B[6]+A[10]*B[10]+A[11]*B[14],
                                 A[8]*B[3]+A[9]*B[7]+A[10]*B[11]+A[11]*B[15],

                                 A[12]*B[0]+A[13]*B[4]+A[14]*B[8]+A[15]*B[12],
                                 A[12]*B[1]+A[13]*B[5]+A[14]*B[9]+A[15]*B[13],
                                 A[12]*B[2]+A[13]*B[6]+A[14]*B[10]+A[15]*B[14],
                                 A[12]*B[3]+A[13]*B[7]+A[14]*B[11]+A[15]*B[15]};
        result = AB;
    }
    return result;
}

///
/// \brief matVecProduct    caluclates the product of a 3x3 matrix and a 3-dim vector
/// \param A                3x3 matrix, saved row-wise
/// \param v                3-dim vector
/// \return
///
std::vector<float> matVecProduct(const std::vector<float>& A, const std::vector<float>& v){
    std::vector<float> result;
    result.push_back(A[0]*v[0]+A[1]*v[1]+A[2]*v[2]);
    result.push_back(A[3]*v[0]+A[4]*v[1]+A[5]*v[2]);
    result.push_back(A[6]*v[0]+A[7]*v[1]+A[8]*v[2]);
    return result;
}

///
/// \brief addVectors   adds two vectors
/// \param v            first vector
/// \param w            second vector
/// \return             v+w
///
std::vector<float> addVectors(const std::vector<float>& v, const std::vector<float>& w){
    std::vector<float> result;
    for(size_t i = 0; i < v.size(); i++){
        result.push_back(v[i] + w[i]);
    }
    return result;
}

///
/// \brief flipVector multiplies a vector with -1
/// \param v          vector
/// \return           -v
///
std::vector<float> flipVector(const std::vector<float>& v){
    std::vector<float> result;
    for(size_t i = 0; i < v.size(); i++){
        result.push_back(-v[i]);
    }
    return result;
}

///
/// \brief transposeMat transposes a 3x3 matrix
/// \param A            3x3 matrix
/// \return
///
std::vector<float> transposeMat(const std::vector<float>& A){
    std::vector<float> result = {A[0], A[3], A[6],
                                 A[1], A[4], A[7],
                                 A[2], A[5], A[8]};
    return result;
}


///
/// \brief calculateTransform multiplies two euclidian transforms
/// \param R_1                3x3 rotation matrix
/// \param R_2                3x3 rotation matrix
/// \param t_1                3-dim translation vector
/// \param t_2                3-dim translation vector
/// \param R                  Result: R_2*R_1
/// \param t                  Result: R_2*t_1 + t_2
///
void calculateTransform(const std::vector<float>& R_1, const std::vector<float>& R_2,
                        const std::vector<float>& t_1, const std::vector<float>& t_2,
                        std::vector<float>* R, std::vector<float>* t){
    *R = matProduct(R_2,R_1);
    *t = addVectors(matVecProduct(R_2,t_1),t_2);
}

///
/// \brief invertTransform  inverts a euclidian transformation
/// \param T                4x4 matrix containing a 3x3 rotation and a
///                         translation vector
/// \return                 T^-1
///
std::vector<float> invertTransform(const std::vector<float>& T){
    std::vector<float> R = {T[0], T[1], T[2], T[4], T[5], T[6], T[8], T[9], T[10]};
    std::vector<float> t = {T[3], T[7], T[11]};
    std::vector<float> R_inv = transposeMat(R);
    std::vector<float> t_inv = matVecProduct(R_inv, t);
    std::vector<float> result = {R_inv[0], R_inv[1], R_inv[2], -t_inv[0],
                                 R_inv[3], R_inv[4], R_inv[5], -t_inv[1],
                                 R_inv[6], R_inv[7], R_inv[8], -t_inv[2],
                                 0,0,0,1};
    return result;
}

/////////////////////////// iostream helper ////////////////////////////////////

///
/// \brief goToLine jumps to a specific line in an ifstream
/// \param file     the ifstream to perform the jump on
/// \param num      line number
/// \return         true, if line exists, false, if eof reached
///
bool goToLine(std::ifstream& file, const unsigned int& num){
    file.seekg(std::ios::beg);
    for(int i=0; i < num - 1; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return !file.eof();
}
