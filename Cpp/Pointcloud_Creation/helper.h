#pragma once

#include <fstream>
#include <vector>

//////////////////////// matrix/vector calculations ////////////////////////////

///
/// \brief matProduct   calculates the product of two 3x3 matrices
/// \param A            3x3 matrix, saved row-wise
/// \param B            3x3 matrix, saved row-wise
/// \return             A*B, row-wise
///
std::vector<float> matProduct(const std::vector<float>& A, const std::vector<float>& B){
    std::vector<float> result;
    result.push_back(A[0]*B[0]+A[1]*B[3]+A[2]*B[6]);
    result.push_back(A[0]*B[1]+A[1]*B[4]+A[2]*B[7]);
    result.push_back(A[0]*B[2]+A[1]*B[5]+A[2]*B[8]);
    result.push_back(A[3]*B[0]+A[4]*B[3]+A[5]*B[6]);
    result.push_back(A[3]*B[1]+A[4]*B[4]+A[5]*B[7]);
    result.push_back(A[3]*B[2]+A[4]*B[5]+A[5]*B[8]);
    result.push_back(A[6]*B[0]+A[7]*B[3]+A[8]*B[6]);
    result.push_back(A[6]*B[1]+A[7]*B[4]+A[8]*B[7]);
    result.push_back(A[6]*B[2]+A[7]*B[5]+A[8]*B[8]);
    return result;
}

///
/// \brief matVecProduct    caluclates the product of a 3x3 matrix and a 3-dim vector
/// \param A                3x3 matrix, saved row-wise
/// \param v                3-dim vector
/// \return
///
std::vector<float> matVecProduct(const std::vector<float>& A, const std::vector<float>& v){
    std::vector<float> result;
    result.push_back(A[0]*v[0]+A[1]*v[1]+A[2]*v[2]);
    result.push_back(A[3]*v[0]+A[4]*v[1]+A[5]*v[2]);
    result.push_back(A[6]*v[0]+A[7]*v[1]+A[8]*v[2]);
    return result;
}

///
/// \brief addVectors   adds two vectors
/// \param v            first vector
/// \param w            second vector
/// \return             v+w
///
std::vector<float> addVectors(const std::vector<float>& v, const std::vector<float>& w){
    std::vector<float> result;
    for(size_t i = 0; i < v.size(); i++){
        result.push_back(v[i] + w[i]);
    }
    return result;
}

///
/// \brief flipVector multiplies a vector with -1
/// \param v          vector
/// \return           -v
///
std::vector<float> flipVector(const std::vector<float>& v){
    std::vector<float> result;
    for(size_t i = 0; i < v.size(); i++){
        result.push_back(-v[i]);
    }
    return result;
}

///
/// \brief transposeMat transposes a 3x3 matrix
/// \param A            3x3 matrix
/// \return
///
std::vector<float> transposeMat(const std::vector<float>& A){
    std::vector<float> result = {A[0], A[3], A[6], A[1], A[4], A[7], A[2], A[5], A[8]};
    return result;
}


///
/// \brief calculateTransform multiplies two euclidian transforms
/// \param R_1                3x3 rotation matrix
/// \param R_2                3x3 rotation matrix
/// \param t_1                3-dim translation vector
/// \param t_2                3-dim translation vector
/// \param R                  Result: R_2*R_1
/// \param t                  Result: R_2*t_1 + t_2
///
void calculateTransform(const std::vector<float>& R_1, const std::vector<float>& R_2,
                        const std::vector<float>& t_1, const std::vector<float>& t_2,
                        std::vector<float>* R, std::vector<float>* t){
    *R = matProduct(R_2,R_1);
    *t = addVectors(matVecProduct(R_2,t_1),t_2);
}


/////////////////////////// iostream helper ////////////////////////////////////

///
/// \brief GotoLine jumps to a specific line in an ifstream
/// \param file     the ifstream to perform the jump on
/// \param num      line number
/// \return         true, if line exists, false, if eof reached
///
bool GotoLine(std::ifstream& file, const unsigned int& num){
    file.seekg(std::ios::beg);
    for(int i=0; i < num - 1; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return !file.eof();
}

////////////////////////////// other helpers ///////////////////////////////////

///
/// \brief rgb2hsv
/// \param r
/// \param g
/// \param b
/// \param h
/// \param s
/// \param v
///
void rgb2hsv(const unsigned char& r, const unsigned char& g, const unsigned char& b,
             unsigned char& h, unsigned char& s, unsigned char& v){
    float r_f = float(r) / 255.0f;
    float g_f = float(g) / 255.0f;
    float b_f = float(b) / 255.0f;

    float h_f, s_f, v_f; // ranges: h:0-360.0, s:0.0-1.0, v:0.0-1.0

    float max = std::max(r_f, std::max(g_f, b_f));
    float min = std::min(r_f, std::min(g_f, b_f));

    v_f = max;

    if (max == 0.0f) {
        s_f = 0.0f;
        h_f = 0.0f;
    }
    else if (max - min == 0.0f) {
        s_f = 0.0f;
        h_f = 0.0f;
    }
    else {
        s_f = (max - min) / max;

        if (max == r_f) {
            h_f = 60.0f * ((g_f - b_f) / (max - min)) + 0.0f;
        }
        else if (max == g) {
            h_f = 60.0f * ((b_f - r_f) / (max - min)) + 120.0f;
        }
        else {
            h_f = 60.0f * ((r_f - g_f) / (max - min)) + 240.0f;
        }
    }

    if (h_f < 0.0f) h_f += 360.0f;

    h = (unsigned char)(h_f / 2.0f);   // h : 0-180
    s = (unsigned char)(s_f * 255.0f); // s : 0-255
    v = (unsigned char)(v_f * 255.0f); // v : 0-255
}
