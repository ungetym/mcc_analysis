#include "helper.h"

#include <k4a/k4a.h>
#include <opencv4/opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/transforms.h>

/////////////////////// calibration file parser ////////////////////////////////

///
/// \brief loadParameters           reads a MCC_Cams file and creates kinect SDK
///                                 calibration structs
/// \param file_name                path to calibration file
/// \param reference_camera_name    name of camera that should be used as
///                                 reference frame
/// \param calibrations             OUTPUT: created k4a calibration structs
/// \return                         list of device names corresponding to the
///                                 calibration structs
///
std::vector<std::string> loadParameters(const std::string& file_name,
                              const std::string& reference_camera_name,
                              std::vector<k4a_calibration_t>* calibrations){

    std::vector<std::string> camera_name_list;

    // open file
    std::ifstream infile(file_name.c_str());
    // check if file can be read
    if(!infile.is_open ())
    {
        printf ("Error opening file %s. Continueing with default calibration\n", file_name.c_str());
        return camera_name_list;
    } else {
        printf("New intrinsic parameters loaded \n");
    }

    // parse camera parameters - while the intrinsics can directly be written
    // into the k4a_calibration_t structs, the transforms require some
    // additional calculations

    // index of current line in parameter file - line 6 is the start of the
    // first camera's description
    int current_line = 6;
    // save index for every camera - this is helpful if the ir and rgb cameras
    // are not stored in a known order
    std::map<std::string,int> name_index;
    // rotations, translations and the inverted pendants for ir and rgb cameras
    // are saved in the following vectors. These are later used to calulate the
    // transforms for the k4a_calibration_t structs
    std::vector<std::vector<float>> rotations_rgb, rotations_inv_rgb,
            rotations_ir, rotations_inv_ir, translations_rgb,
            translations_inv_rgb, translations_ir, translations_inv_ir;

    // save the inverse transform of the reference camera
    std::vector<float> rotation_inv_reference;
    std::vector<float> translation_inv_reference;

    // start reading
    while(GotoLine(infile, current_line)){

        // extract camera name
        std::string camera_name;
        infile >> camera_name;
        infile >> camera_name;

        if(infile.eof()){
            break;
        }

        // check if reference camera
        bool is_ref_camera = (camera_name.compare(reference_camera_name) == 0);

        // check if ir or rgb camera
        std::string prefix = camera_name.substr(0,2);
        bool is_ir_camera = (prefix.compare("ir") == 0);
        if(is_ir_camera){
            camera_name = camera_name.substr(2);
        }
        // search for camera name in existing cameras
        int idx = -1;
        if(name_index.count(camera_name) > 0){
            idx = name_index[camera_name];
        }
        else{ // if camera is not found, create a new one
            calibrations->push_back(k4a_calibration_t());
            idx = calibrations->size()-1;
            camera_name_list.push_back(camera_name);
            name_index[camera_name] = idx;
        }

        k4a_calibration_t* calibration = &(*calibrations)[idx];
        float var;

        if(is_ir_camera){

            // jump to next line in which the ir camera intrinsics should be located
            GotoLine(infile, current_line+1);
            // read ir camera parameters
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.k1;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.k2;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.p1;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.p2;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.k3;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.k4;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.k5;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.k6;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.fx;
            infile>>var;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.cx;
            infile>>var;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.fy;
            infile>>calibration->depth_camera_calibration.intrinsics.parameters.param.cy;
        }
        else{
            // jump to next line in which the rgb camera intrinsics should be located
            GotoLine(infile, current_line+1);
            // read rgb camera parameters
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.k1;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.k2;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.p1;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.p2;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.k3;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.k4;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.k5;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.k6;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.fx;
            infile>>var;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.cx;
            infile>>var;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.fy;
            infile>>calibration->color_camera_calibration.intrinsics.parameters.param.cy;
        }

        // jump two lines to the rgb or ir camera extrinsics
        GotoLine(infile, current_line+3);
        // read pose (saved row-wise)
        // Attention: The Kinect SDK seems to interpret the transformations
        //            inversely compared to the calibration tool. A transformation
        //            R, t in the SDK transformes the a point x from the source
        //            coordinate system into the target system by performing the
        //            operation [R t]*x instead of [R^-1 -R^-1*t]*x (as used in the
        //            calibration tool).

        std::vector<float> r_inv(9); // inverted rotation
        std::vector<float> t_inv(3); // inverted translation
        infile >> r_inv[0];
        infile >> r_inv[1];
        infile >> r_inv[2];
        infile >> t_inv[0];
        infile >> r_inv[3];
        infile >> r_inv[4];
        infile >> r_inv[5];
        infile >> t_inv[1];
        infile >> r_inv[6];
        infile >> r_inv[7];
        infile >> r_inv[8];
        infile >> t_inv[2];

        // calculate inverse transformation
        std::vector<float> r = transposeMat(r_inv);
        std::vector<float> t = flipVector(matVecProduct(r,t_inv));

        // save transformations
        if(is_ir_camera){
            rotations_ir.push_back(r);
            rotations_inv_ir.push_back(r_inv);
            translations_ir.push_back(t);
            translations_inv_ir.push_back(t_inv);
        }
        else{
            rotations_rgb.push_back(r);
            rotations_inv_rgb.push_back(r_inv);
            translations_rgb.push_back(t);
            translations_inv_rgb.push_back(t_inv);
        }

        if(is_ref_camera){
            rotation_inv_reference = r_inv;
            translation_inv_reference = t_inv;
        }

        current_line += 4;
    }

    // loop over all cameras and set the transformations between color and ir cameras
    for(size_t cam_idx = 0; cam_idx < calibrations->size(); cam_idx++){
        k4a_calibration_t* calibration = &(*calibrations)[cam_idx];

        // calculate transform from color to depth R_ir^t*(R_rgb*x+t_rgb)-t_ir)
        std::vector<float> r_rgb_to_ir = matProduct(rotations_inv_ir[cam_idx],rotations_rgb[cam_idx]);
        std::vector<float> t_rgb_to_ir = addVectors(matVecProduct(rotations_inv_ir[cam_idx],
                                                                  translations_rgb[cam_idx]),
                                                    translations_inv_ir[cam_idx]);

        // and the inverse
        std::vector<float> r_ir_to_rgb = transposeMat(r_rgb_to_ir);
        std::vector<float> t_ir_to_rgb = flipVector(matVecProduct(r_ir_to_rgb, t_rgb_to_ir));

        // calculate the transform from rgb to reference camera
        std::vector<float> r_rgb_to_ref = matProduct(rotation_inv_reference,rotations_rgb[cam_idx]);
        std::vector<float> t_rgb_to_ref = addVectors(matVecProduct(rotation_inv_reference,
                                                                   translations_rgb[cam_idx]),
                                                     translation_inv_reference);

        // set the respective transformations to the different extrinsics-structs

        std::copy(r_rgb_to_ir.begin(), r_rgb_to_ir.end(),
                  calibration->extrinsics[K4A_CALIBRATION_TYPE_COLOR][K4A_CALIBRATION_TYPE_DEPTH].rotation);
        std::copy(t_rgb_to_ir.begin(), t_rgb_to_ir.end(),
                  calibration->extrinsics[K4A_CALIBRATION_TYPE_COLOR][K4A_CALIBRATION_TYPE_DEPTH].translation);

        std::copy(r_ir_to_rgb.begin(), r_ir_to_rgb.end(),
                  calibration->extrinsics[K4A_CALIBRATION_TYPE_DEPTH][K4A_CALIBRATION_TYPE_COLOR].rotation);
        std::copy(t_ir_to_rgb.begin(), t_ir_to_rgb.end(),
                  calibration->extrinsics[K4A_CALIBRATION_TYPE_DEPTH][K4A_CALIBRATION_TYPE_COLOR].translation);

        std::copy(r_rgb_to_ref.begin(), r_rgb_to_ref.end(),
                  calibration->color_camera_calibration.extrinsics.rotation);
        std::copy(t_rgb_to_ref.begin(), t_rgb_to_ref.end(),
                  calibration->color_camera_calibration.extrinsics.translation);

        // set further calibration properties (usually loaded from the device if connected)
        calibration->color_camera_calibration.intrinsics.type =
                K4A_CALIBRATION_LENS_DISTORTION_MODEL_BROWN_CONRADY;
        calibration->depth_camera_calibration.intrinsics.type =
                K4A_CALIBRATION_LENS_DISTORTION_MODEL_BROWN_CONRADY;
        calibration->color_camera_calibration.intrinsics.parameter_count = 14;
        calibration->depth_camera_calibration.intrinsics.parameter_count = 14;
        calibration->color_resolution = K4A_COLOR_RESOLUTION_720P;
        calibration->color_camera_calibration.resolution_width = 1280;
        calibration->color_camera_calibration.resolution_height = 720;
        calibration->depth_mode = K4A_DEPTH_MODE_WFOV_UNBINNED;
        calibration->depth_camera_calibration.resolution_width = 1024;
        calibration->depth_camera_calibration.resolution_height = 1024;

        calibration->color_camera_calibration.metric_radius = 1.3;//?
        calibration->depth_camera_calibration.metric_radius = 1.3f;//?

    }

    return camera_name_list;

}




////////////////////////////// main program ////////////////////////////////////

int main(){

    /////////////////////  set paths to calibration and images  ////////////////

    // path to the data folder containing the image and calibration files
    std::string input_path ="/home/anonym/Projects/Ant_Media/multi_params_in_k4a/input/";
    // name of the calibration file
    std::string calibration_file_name ="final_all.MCC_Cams";
    // image id
    std::string image_idx ="4592";
    // path to output dir
    std::string output_path ="/home/anonym/Projects/Ant_Media/multi_params_in_k4a/output/";
    // name of the desired reference camera
    std::string reference_camera_name = "irkinect0";

    //////////////////  load parameters and create transforms  /////////////////

    std::vector<k4a_calibration_t> calibrations;
    std::vector<std::string> camera_names = loadParameters(input_path+calibration_file_name,
                                                           reference_camera_name, &calibrations);

    std::vector<k4a_transformation_t> transforms;
    for(const k4a_calibration_t& calibration : calibrations){
        transforms.push_back(k4a_transformation_create(&calibration));
    }


    ////  use the kinect SDK to transform the depth images into pointclouds ////

    for(size_t cam_idx = 0; cam_idx < calibrations.size(); cam_idx++){

        std::string camera_name = camera_names[cam_idx];
        // load color image
        cv::Mat color = cv::imread(input_path+camera_name+"_"+image_idx+".png");
        // load depth image
        cv::Mat depth = cv::imread(input_path+"depth"+camera_name+"_"+image_idx+".png",
                                   cv::IMREAD_ANYDEPTH);

        // create k4a depth image from cv::Mat
        k4a_image_t k4a_depth = NULL;
        if (K4A_RESULT_SUCCEEDED !=k4a_image_create_from_buffer(K4A_IMAGE_FORMAT_DEPTH16,
                                                                depth.cols, depth.rows,
                                                                depth.cols * sizeof(uint16_t),
                                                                (uint8_t*)depth.data,
                                                                depth.cols* depth.rows* sizeof(uint16_t),
                                                                NULL, NULL, &k4a_depth)){
            printf("Failed to create depth image\n");
            return 0;

        }


        ////////////  create transformed depth image in rgb camera space  //////

        k4a_image_t transformed_depth_image = NULL;
        if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_DEPTH16,
                                                     color.cols,
                                                     color.rows,
                                                     color.cols * (int)sizeof(uint16_t),
                                                     &transformed_depth_image)){
            printf("Failed to create transformed depth image\n");
            return 0;
        }

        if (K4A_RESULT_SUCCEEDED !=
                k4a_transformation_depth_image_to_color_camera(transforms[cam_idx], k4a_depth,
                                                               transformed_depth_image)){
            printf("Failed to compute transformed depth image\n");
            return 0;
        }

        // create cv::Mat from k4a transformed
        //cv::Mat transformed_depth_image_cv(color.rows, color.cols, CV_16UC1,
        //                                   (void *)k4a_image_get_buffer(transformed_depth_image));
        //cv::imwrite(output_path+"transformed.png", transformed_depth_image_cv);


        /////////////////  create pointcloud from transformed image  ///////////

        k4a_image_t point_cloud_image = NULL;
        if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM,
                                                     color.cols,
                                                     color.rows,
                                                     color.cols * 3 * (int)sizeof(int16_t),
                                                     &point_cloud_image))
        {
            printf("Failed to create point cloud image\n");
            return 0;
        }

        if (K4A_RESULT_SUCCEEDED !=
                k4a_transformation_depth_image_to_point_cloud(transforms[cam_idx],
                                                              transformed_depth_image,
                                                              K4A_CALIBRATION_TYPE_COLOR,
                                                              point_cloud_image))
        {
            printf("Failed to compute point cloud\n");
            return 0;
        }

        // create cv::Mat from k4a pointcloud
        cv::Mat point_cloud(color.rows, color.cols, CV_16SC3, (void *)k4a_image_get_buffer(point_cloud_image));
        //cv::imwrite(output_path+"pcl.png",point_cloud);


        ////////////////////////////  export to pcl  ///////////////////////////

        pcl::PointCloud<pcl::PointXYZRGBA> cloud;

#pragma omp parallel for
        for(int r = 0; r < color.rows; ++r){
            const cv::Vec3s *itD =  point_cloud.ptr<cv::Vec3s>(r);
            const cv::Vec3b *itC = color.ptr<cv::Vec3b>(r);

            for(size_t c = 0; c < color.cols; ++c, ++itD ,++itC ){

                const float x = float(itD->val[0]) /1000.0f ;
                const float y = float(itD->val[1]) /1000.0f ;
                const float z = float(itD->val[2]) /1000.0f ;

                // Check for invalid measurements
                if(z == 0 || x==0 || y==0 || isnan(x) || isnan(y)){
                    continue;
                }

                unsigned char h,s,v;
                rgb2hsv((unsigned char)itC->val[2], (unsigned char)itC->val[1],
                        (unsigned char)itC->val[0], h, s, v);
                if (s < 255 && s > 52 && v < 255 && v > 72) {
                    continue;
                }

                pcl::PointXYZRGBA p;
                p.z = z;
                p.x = x;
                p.y = y;
                p.b = itC->val[0];
                p.g = itC->val[1];
                p.r = itC->val[2];
                p.a = 255;
                cloud.push_back(p);
            }
        }
        // as a final step, transform the pointcloud from the current rgb cam
        // into the reference camera coordinate system - the necessary
        // transformation was saved before in calibration->color_camera_...
        // ...calibration.extrinsics.rotation and .translation
        Eigen::Matrix3f r_rgb_to_ref = Eigen::Map<Eigen::Matrix<float,3,3,Eigen::RowMajor>>(
                    calibrations[cam_idx].color_camera_calibration.extrinsics.rotation);
        Eigen::Vector3f t_rgb_to_ref = Eigen::Map<Eigen::Matrix<float,3,1>>(
                    calibrations[cam_idx].color_camera_calibration.extrinsics.translation);
        Eigen::Matrix4f trafo_rgb_to_ref = Eigen::Matrix4f::Identity();
        trafo_rgb_to_ref.block<3,3>(0,0) = r_rgb_to_ref;
        trafo_rgb_to_ref.block<3,1>(0,3) = t_rgb_to_ref * 0.001f;
        pcl::transformPointCloud(cloud, cloud, trafo_rgb_to_ref);
        pcl::io::savePLYFileBinary(output_path+camera_name+"_cloud.ply", cloud);
        printf("Pointcloud for %s successfully created!\n", camera_name.c_str());
    }

    return 0;
}
